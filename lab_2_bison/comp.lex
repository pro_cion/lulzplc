%{
    #include "bison.tab.h"
	int columnNumber = 0;
%}

DIGIT [0-9]
ID    [A-Za-z_][A-Za-z0-9_]*

%x comment
%x string

%%
module    return MODULE; columnNumber += yyleng;
end       return END; columnNumber += yyleng;
break     return BREAK; columnNumber += yyleng;
continue  return CONTINUE; columnNumber += yyleng;
for       return FOR; columnNumber += yyleng;
if        return IF; columnNumber += yyleng;
else      return ELSE; columnNumber += yyleng;
"{"       return LBRACE; columnNumber += yyleng;
"}"       return RBRACE; columnNumber += yyleng;
"("       return LPAREN; columnNumber += yyleng;
")"       return RPAREN; columnNumber += yyleng;
procedure return PROC; columnNumber += yyleng;
"["       return LBRACK; columnNumber += yyleng;
"]"       return RBRACK; columnNumber += yyleng;
out       return OUT; columnNumber += yyleng;
var       return VAR; columnNumber += yyleng;
scan      return SCAN; columnNumber += yyleng;
print     return PRINT; columnNumber += yyleng;
goto      return GOTO; columnNumber += yyleng;
integer   return INTEGER; columnNumber += yyleng;
char      return CHAR; columnNumber += yyleng;

","       return COMMA; columnNumber += yyleng;
"."       return DOT; columnNumber += yyleng;
";"       return SCOLON; columnNumber += yyleng;
":"       return COLON; columnNumber += yyleng;

">="      return MORE_EQ; columnNumber += yyleng;
"<="      return LESS_EQ; columnNumber += yyleng;
"=="      return EQUAL; columnNumber += yyleng;
"!="      return NOT_EQUAL; columnNumber += yyleng;
">"       return MORE; columnNumber += yyleng;
"<"       return LESS; columnNumber += yyleng;


"+"       return PLUS; columnNumber += yyleng;
"-"       return MINUS; columnNumber += yyleng;
"="       return ASSIGN; columnNumber += yyleng;
"*"       return MULT; columnNumber += yyleng;
"/"       return DIVISION; columnNumber += yyleng;
"mod"     return MOD; columnNumber += yyleng;
"and"     return AND; columnNumber += yyleng;
"or"      return OR; columnNumber += yyleng;
"not"     return NOT; columnNumber += yyleng;


\"        BEGIN(string); columnNumber += yyleng;
<string>\\\"      columnNumber += yyleng;
<string>\"        BEGIN(INITIAL); columnNumber ++;
<string>\n        columnNumber = 0;
<string>.         columnNumber += yyleng;

{DIGIT}+  return NUMBER; columnNumber += yyleng;
{ID}      return IDENT; columnNumber += yyleng;

"//"+[^\n]*	columnNumber += yyleng;

"/*"      BEGIN(comment); columnNumber += yyleng;
<comment>\n columnNumber = 0;
<comment>[^*\n]* columnNumber += yyleng;
<comment>"*"+[^*/\n]* columnNumber += yyleng;
<comment>"*/" BEGIN(INITIAL); columnNumber += yyleng;

\n         columnNumber = 0;
.          ++columnNumber;  

%%

int yywrap() {}


int yyerror(const char *str)
{
    print("ERROR \"%s\" on %d:%d", str, lineNumber, columnNumber);

/*
void main(int argc, char **argv)
{
    printf("proC lexical parser written with the help of flex\n");
    argc--;argv++;
    if (argc > 0)
    {
        yyin = fopen(argv[0], "r");
        if (yyin == NULL)
        {
            printf("Cannot open file specified. yyin = stdin");
            yyin = stdin;
        }
    }
    else
        yyin = stdin;
    yylex();
}
*/
