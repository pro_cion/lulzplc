%{
	#include stdlib.h
	#include stdio.h
	extern int yylineno;
	extern int columnNumber;
	extern int yylex();
%}

%defines

%token INT CHAR IF ELSE DO PRINT FOR SCAN RETURN PROCEDURE
%token NUMBER IDENT COMMA SCOLON
%token LBRACKET RBRACKET LBRACE RBRACE LPAREN RPAREN END COMMENT
%token EQUAL ASSIGN LESS_EQ MORE_EQ MORE LESS NOT_EQUAL OR AND
%token PLUS MINUS MUL DIV

%start MODULE
%left PLUS MINUS
%left MULT DIV
%left LPAREN RPAREN
%left uminus
%left OR AND NOT
%error-verbose
%%

module:
		MODULE IDENT SCOLON
	    	proc_decl_block
		stmts_block
		END;

var_qualfr: 
            empty
            | CONST;

stmts_block:
            statement;

passing_qual:
            OUT
	    | VAR;

proc_decl_block: 
            empty
            | procedure_decl;
            
procedure_decl:
            procedure_decl
            | procedure_head SEMICOLON stmts_block END;
            
procedure_head: 
            PROC IDENT
	    | PROC IDENT LPAREN passing_qual IDENT COLON type RPAREN;

proc_args:
            passsing_qual IDENT COLON type
            | proc_args COMMA passing_qual IDENT COLON type;
            
variable_decl:
            var_qualfr type IDENT SCOLON
            | var_qualfr type IDENT LBRACK unsigned_int RBRACK SCOLON
	    | type assignment_stmt;
				  
statement:
	    empty
            | unlabelled_stmt
            | label : unlabelled_stmt;
            
unlabelled_stmt:
            simple_stmt SCOLON
            | structured_stmt SCOLON
            | structured_stmt;
            
simple_stmt: 
            goto_stmt
            | assignment_stmt
            | procedure_call;
            
structured_stmt:
            if-stmt
            | for-loop-stmt
            
procedure-call:
            IDENT LPAREN expression RPAREN
            | builtin-proc;
            
builtin-proc:
            scan 
            | print;
    
goto-stmt:
            goto label;
    
for-loop-stmt:
            FOR VAR ASSIGN expression COMMA expresssion COMMA statement stmts-block END;
            
if-stmt:
            IF expression stmts-block ELSE stmts-block END
            | IF expression stmts-block END;
            
assignment-stmt:
            variable ASSIGN expression;
            
scan:
            SCAN variable SEMICOLON;
print:
            PRINT expression;
empty:
        ;

sign:
        PLUS
        | MINUS;
         
         
unary-oprt:
         sign
         | NOT;
         
expression:
            logical-or-expr
            | expression relational-oper expression
            
/*
            simple-expr
            | simple-expr relational-oper simple-expr;*/
                             
relational-oprt:
            EQUAL
            | NOT_EQUAL
            | LESS
            | MORE
            | LES_EQ
            | MORE_EQ;

logical_or_expr:
            logical_or_expr OR logical_and_expr
            | logical_and_expr;
            
logical_and_expr:
            logical_and_expr AND add_expr 
            | add-expr;
            
add-expr:
            plus-expr adding-oprt mult-expr
            | mult-expr;
            
mult_expr:
            
                      
            /*simple-expr adding-oper term | term | unary-oprt term | 
adding-oprt ::= '+' | '-' | or
term ::= factor | term multpl-oprt factor
multpl-oprt ::= '*' | '/' | mod | and 
factor ::= variable | constant  | '(' expression ')'
*/
variable ::= entire-variable | array-variable
entire-variable ::= IDENT
array-variable ::= IDENT '[' expression ']'
label ::= IDENT
type:
    integer
    | char

mult-delimiter ::= { delimiter }+
delimiter ::= newline | ' ' | '\t' 

number ::= integer-number
integer-number ::= digit-seq
constant ::= [sign] number | string
digit-seq ::= [sign] unsigned-digit-seq
unsigned-digit-seq ::= { digit }+
character ::= any-character

newline ::= '\n' | '\r' | \r\n
digit ::= '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9'
%%

