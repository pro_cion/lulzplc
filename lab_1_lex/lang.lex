%{
	#include <string.h>
	int lineNumber = 1;
	int columnNumber = 0;
    /*L?\"(\\.|[^\\"])*\" { count(); return(STRING_LITERAL); }*/
%}

DIGIT [0-9]
ID    [A-Za-z_][A-Za-z0-9_]*

%x comment
%x string

%%
module    printf("module on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
end       printf("end on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
break     printf("break on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
continue  printf("continue on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
for       printf("for on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
if        printf("if on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
else      printf("else on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
do        printf("do on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
then      printf("then on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
"{"       printf("rbrace on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
"}"       printf("lbrace on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
"("       printf("lparen on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
")"       printf("rparen on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
procedure printf("proc on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
"["       printf("lbracket on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
"]"       printf("rbracket on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
out       printf("out on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
var       printf("var on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
scan      printf("scan on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
print     printf("print on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
goto      printf("goto on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
integer   printf("integer on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
char      printf("char on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;

","       printf("comma on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
"."       printf("dot on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
";"       printf("semicol on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
":"       printf("colon on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;

">="      printf("greater or eq on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
"<="      printf("less or eq on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
"=="      printf("comparison on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
"!="      printf("not equals on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
">"       printf("more on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
"<"       printf("less on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;

"+"       printf("plus on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
"-"       printf("minus on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
"="       printf("equals on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
"*"       printf("mult on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
"/"       printf("div on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
"mod"     printf("mod on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
"and"     printf("and on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
"or"      printf("or on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;


\"        BEGIN(string); printf("str begin on %d:%d", lineNumber, columnNumber); columnNumber += yyleng;
<string>\\\"      columnNumber += yyleng;
<string>\"        BEGIN(INITIAL); printf("str end on %d:%d", lineNumber, columnNumber); columnNumber ++;
<string>\n        ++lineNumber; columnNumber = 0;
<string>.         columnNumber += yyleng;

{DIGIT}+  printf("digit: %s on %d:%d\n", yytext, lineNumber, columnNumber); columnNumber += yyleng;
{ID}      printf("identifier: %s on %d:%d\n",yytext, lineNumber, columnNumber); columnNumber += yyleng;

"/*"      BEGIN(comment); printf("coment begins on %d:%d\n", lineNumber, columnNumber); columnNumber += yyleng;
<comment>\n ++lineNumber; columnNumber = 0;
<comment>[^*\n]* columnNumber += yyleng;
<comment>"*"+[^*/\n]* columnNumber += yyleng;
<comment>"*"+"/" BEGIN(INITIAL); printf("comment ended\n"); columnNumber += yyleng;

\n         ++lineNumber; columnNumber = 0;
.          ++columnNumber;  

%%
void main(int argc, char **argv)
{
    printf("proC lexical parser written with the help of flex\n");
    argc--;argv++;
    if (argc > 0)
    {
        yyin = fopen(argv[0], "r");
        if (yyin == NULL)
        {
            printf("Cannot open file specified. yyin = stdin");
            yyin = stdin;
        }
    }
    else
        yyin = stdin;
    yylex();
}
