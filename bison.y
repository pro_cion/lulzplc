%{
	#include <stdlib.h>
	#include <stdio.h>
	extern int yylineno;
	extern int yylex();



%}

%defines

%token VOID INT CHAR IF ELSE WHILE DO PRINT FOR SCAN RETURN STRING MAIN
%token number identifier semicolon comma quest
%token left_bracket right_bracket left_sqBracket right_sqBracket begin end comment
%token equal compare less_equal more_equal more less not_equal not or and
%token plus minus mul division plus_equal minus_equal div_equal mul_equal inc dec

%start module
/*%left plus minus plus_equal minus_equal
%left mul division div_equal mul_equal
%left left_bracket right_bracket
%left inc dec
%left uminus
%left or and not
%error-verbose
*/
%%
module:
		module <ident> <block> end

<var-qualfr> ::= [<const>]
<block> ::= <statement>

<passing-qual> ::= out | var
<procedure-decl> ::= <procedure-head> <block>
<procedure-head> ::= procedure <ident>
			       | procedure <ident> '(' <passing-qual> <ident> ':' <type> ')'

#statements

<variable-decl> ::= <var-qualfr> <type> <ident> [ '[' <unsigned-int> ']' ]
				  | <type> <assignment statement>
<statement> ::= <unlabelled-stmt> | <label> : <unlabelled-stmt>
<unlabelled-stmt> ::= <simple-stmt> | <structured-stmt> 
<simple-stmt> ::= <goto-stmt> | <assignment-stmt> | <procedure-call>
			    | <empty-statement>
<procedure-call> ::= <ident> '(' <expression> ')' | <built-proc>
<built-proc> ::= <scan> | <print>
<goto-stmt> ::= goto <label>
<for-loop-stmt> ::= for <var> '=' <expression> ',' <expresssion> ','<statement> <block> end
<if-stmt> ::= if <expression> <statement> [ else <statement> ] end
<assignment-stmt> ::= <var> '=' <expression>
<scan> ::= scan <variable>
<print> ::= print <expression>
<empty-statement> ::= <empty>
<empty> ::= 

#expressions
<sign> ::= '+' | '-'
<expression> ::= <simple-expr> | <simple-expr> <relational-oper> <simple-expr>
<relational-oprt> ::= == | != | '<' | '>' | <= | >=
<simple-expr> ::= <term> | <sign> <term> | <simple-expr> <adding-oper> <term>
<adding-oprt> ::= '+' | '-' | or
<term> ::= <factor> | <term> <multpl-oprt> <factor>
<multpl-oprt> ::= '*' | '/' | mod | and 
<factor> ::= <variable> | <constant>  | '(' <expression> ')'

<variable> ::= <entire-variable> | <array-variable>
<entire-variable> ::= <ident>
<array-variable> ::= <ident> '[' <expression> ']'
<label> ::= <ident>
<type> ::= integer | char
<ident> ::= <letter> |'_' { <letter> | <digit> | '_' }

<mult-delimiter> ::= { <delimiter> }+
<delimiter> ::= <newline> | ' ' | '\t' 

<number> ::= integer-number
<integer-number> ::= <digit-seq>
<constant> ::= [sign] number | <string>
<digit-seq> ::= [sign] <unsigned-digit-seq>
<unsigned-digit-seq> ::= { <digit> }+
<character> ::= any-character

<newline> ::= '\n' | '\r' | \r\n
<digit> ::= '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9'
<letter> ::= 'A' | .. | 'Z' | 'a' | .. | 'z'
%%

